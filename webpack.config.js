var path = require('path')
var webpack = require("webpack")
var BrowserSyncPlugin = require('browser-sync-webpack-plugin')
var BowerWebpackPlugin = require("bower-webpack-plugin")

    module.exports = {
        entry: './assets/app/app.js',
        output: {
            path: __dirname + '/public/js',
            filename: 'app.js'
        },
        resolve: {
            root: [path.join(__dirname, "bower_components")]
        },
        module: {
            loaders: [{
              test: /\.js?$/,
              exclude: /(node_modules|bower_components)/,
              loader: 'babel',
              query: {
                // cacheDirectory: true,
                // optional: ['runtime'],
                // stage: 0
              }
            }]
        },
        devtool: 'source-map',

        plugins: [
            new webpack.ProvidePlugin({
                $: "jquery",
                jQuery: "jquery",
                "window.jQuery": "jquery"

            }),
            new BowerWebpackPlugin({
    			modulesDirectories: ['bower_components'],
    			manifestFiles: ['bower.json', '.bower.json'],
    			includes: /.*/,
    			excludes: /.*\.less$/
    		})
        ]

}
