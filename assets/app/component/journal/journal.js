export function journal (){
    require('jquery')

    $('.buttonJournalClose').click(function () {
        $('.journal').removeClass('journalOpen')
    })
    $('.js-openJournal').click(function () {
        $('.journal').toggleClass('journalOpen')
    })

    var Ps = require('perfect-scrollbar')
    var containerComplete = document.getElementById('js-containerComplete')
    var containerTask = document.getElementById('js-containerTask')
    Ps.initialize(containerTask)
    $('.journalContentTab').click(function () {

        $('.journalContentTab').removeClass('journalContentTabActive')
        $(this).addClass('journalContentTabActive')
        var tabs = $(this).attr('data-tab')

        if(tabs == 'js-containerCompleteShow') {
            $('#js-containerTask').hide()
            $('#js-containerComplete').show()
            Ps.initialize(containerComplete)
        }
        if( tabs == 'js-containerTask') {
            $('#js-containerComplete').hide()
            $('#js-containerTask').show()
            Ps.initialize(containerTask)
        }

    })
}
