export function calendar() {
    require('jquery')
    require('./bootstrap-datepicker.js')
    require('./locales/bootstrap-datepicker.ru.js')
    $('#datepicker').datepicker({
        language: 'ru',
        format: 'dd-mm-yyyy',
        //todayHighlight: true,
        daysOfWeekHighlighted: [0,6],
        beforeShowDay: function (date){
                         if (date.getMonth() == (new Date()).getMonth())
                           switch (date.getDate()){
                             case 4:
                               return "hot"
                             case 8:
                               return "blue";
                             case 12:
                               return "blue";
                         }

                       }

    }).on("changeDate", function (e) {
        $('#datepickerInput').val(
            $('#datepicker').datepicker('getFormattedDate')
        )
    })
}