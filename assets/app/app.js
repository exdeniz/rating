import { journal } from './component/journal/journal.js'
import { calendar } from './component/calendar/calendar.js'
import { count } from './component/count/count.js'
import { personal } from './component/count/personal.js'

document.addEventListener("DOMContentLoaded", function() {
    
    calendar();
    count()
    personal();
    journal()
});
